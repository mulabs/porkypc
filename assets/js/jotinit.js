JotForm.init(function(){ 
    JotForm.calendarMonths = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August","September","October","November","December"]; 
    JotForm.calendarDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]; 
    JotForm.calendarOther = {"today":"Today"}; 
    
    var languageOptions = document.querySelectorAll('#langList li'); 
    for(var langIndex = 0; langIndex < languageOptions.length; langIndex++) { 
        languageOptions[langIndex].on('click', function(e) { 
            setTimeout(function(){ 
                JotForm.setCalendar("6", false, 
                 {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},
                 "future":true,
                 "past":true,
                 "custom":false,
                 "ranges":false,
                 "start":"",
                 "end":""}
                ); 
            }, 0); 
        }); 
    } 
    JotForm.setCalendar("6", false,  
        {"days":{"monday":true,"tuesday":true,"wednesday":true,"thursday":true,"friday":true,"saturday":true,"sunday":true},
        "future":true,
        "past":true,
        "custom":false,
        "ranges":false,
        "start":"",
        "end":""}); 
    JotForm.alterTexts(undefined); 
    JotForm.clearFieldOnHide="disable"; 
    JotForm.submitError="jumpToFirstError"; 
    /*INIT-END*/ 
}); 
JotForm.prepareCalculationsOnTheFly(
    [null,{"name":"heading","qid":"1","text":"Porky Lead","type":"control_head"},
     {"name":"submit2","qid":"2","text":"Reserve Now","type":"control_button"},
     {"description":"","name":"name","qid":"3","text":"Name","type":"control_fullname"},
     {"description":"","name":"email","qid":"4","subLabel":"example@example.com","text":"Email","type":"control_email"},
     {"description":"","name":"phoneNumber","qid":"5","text":"Phone Number","type":"control_phone"},
     {"description":"","name":"date","qid":"6","text":"Date","type":"control_datetime"},
     {"description":"","name":"typeA","qid":"7","text":"Number of Persons","type":"control_checkbox"}
    ]); 
setTimeout(function() {
    JotForm.paymentExtrasOnTheFly([null,
        {"name":"heading","qid":"1","text":"Porky Lead","type":"control_head"},
        {"name":"submit2","qid":"2","text":"Reserve Now","type":"control_button"},
        {"description":"","name":"name","qid":"3","text":"Name","type":"control_fullname"},
        {"description":"","name":"email","qid":"4","subLabel":"example@example.com","text":"Email","type":"control_email"},
        {"description":"","name":"phoneNumber","qid":"5","text":"Phone Number","type":"control_phone"},
        {"description":"","name":"date","qid":"6","text":"Date","type":"control_datetime"},
        {"description":"","name":"typeA","qid":"7","text":"Number of Persons","type":"control_checkbox"}]
        );
}, 20); 