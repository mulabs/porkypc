AOS.init({
  duration: 350
});
  
(function($) {

	'use strict';
  const $menu = $('.site-menu');
  const $burger = $('.site-nav-toggle');
  const headerHeight = $('.site-header').height();

  $(window).stellar({
    responsive: false,
    parallaxBackgrounds: true,
    parallaxElements: true,
    horizontalScrolling: false,
    hideDistantElements: false,
    scrollProperty: 'scroll'
  });

  // loader
  var loader = function() {
    setTimeout(function() { 
      if($('#loader').length > 0) {
        $('#loader').removeClass('show');
      }
    }, 1);
  };
  loader();

  
  // navigation
  var OnePageNav = function() {
    var navToggler = $('.js-site-nav-toggle');
    $(".smoothscroll[href^='#'], #ppx-navbar ul li a[href^='#']").on('click', function(e) {
      e.preventDefault();
      var hash = this.hash;
      var scrollToPosition = $(hash).offset().top - headerHeight;
      $('html, body').animate({ scrollTop: $(hash).offset().top }, 700, 'easeInOutExpo', function(){
        window.location.hash = "" + hash;
        // This hash change will jump the page to the top of the div with the same id
        // so we need to force the page to back to the end of the animation
        $('html').animate({ 'scrollTop': scrollToPosition }, 0);
      });
    });
    $("#ppx-navbar ul li a[href^='#']").on('click', function(e){
      if ( navToggler.is(':visible') ) {
        navToggler.click();
      }
    });
  };
  OnePageNav();

  $(window).scroll(function() {

    var $this = $(this),
      st = $this.scrollTop(),
      navbar = $('.site-header');

    if (st > 150) {
      if ( !navbar.hasClass('scrolled') ) {
        navbar.addClass('scrolled');  
      }
    } 
    if (st < 150) {
      if ( navbar.hasClass('scrolled') ) {
        navbar.removeClass('scrolled sleep');
      }
    } 
    if ( st > 350 ) {
      if ( !navbar.hasClass('awake') ) {
        navbar.addClass('awake'); 
      }
    }
    if ( st < 350 ) {
      if ( navbar.hasClass('awake') ) {
        navbar.removeClass('awake');
        navbar.addClass('sleep');
      }
    }
  }); 

  $('.js-site-nav-toggle').on('click', function(e) {
    var $this = $(this);
    e.preventDefault();
    if ( $('body').hasClass('menu-open') ) {
      $this.removeClass('active');
      $('.site-menu .site-menu-inner > ul > li').each(function(i) {
        var that = $(this);
        setTimeout(function() {
          that.removeClass('is-show');
        }, i * 100);
        // $(this).removeClass('is-show');
      });
      setTimeout(function() {
        // $('.site-menu').fadeOut(400);
        $('.site-menu').removeClass('site-menu-show');
      }, 800);
      $('body').removeClass('menu-open');
    } else {
      // $('.site-menu').fadeIn(400);
      $('.site-menu').addClass('site-menu-show');
      $this.addClass('active');
      $('body').addClass('menu-open');

      setTimeout(function() {
        $('.site-menu .site-menu-inner > ul > li').each(function(i) {
          var that = $(this);
          setTimeout(function() {
            that.addClass('is-show');
          }, i * 100);
        });
      }, 500); 
    }
  });
  $(document).mouseup(function (e) {
     // if the target of the click isn't the container...
    if (!$menu.is(e.target) && $menu.has(e.target).length === 0) 
    // ... or a descendant of the container
    {
      $('body').removeClass('menu-open');
      $menu.removeClass('site-menu-show');
      $burger.removeClass('active');
    }
  });
	$('nav .dropdown').hover(function(){
		var $this = $(this);
		$this.addClass('show');
		$this.find('> a').attr('aria-expanded', true);
		$this.find('.dropdown-menu').addClass('show');
	}, function(){
		var $this = $(this);
			$this.removeClass('show');
			$this.find('> a').attr('aria-expanded', false);
			$this.find('.dropdown-menu').removeClass('show');
	});

  $('#date').datepicker({
    'format': 'm/d/yyyy',
    'autoclose': true
  });

})(jQuery);